# GPaymentTool

#### 介绍
安卓游戏包、渠道计费包，反编译、修改、混包工具。（用于生成可正式付费的游戏apk）

#### 软件架构
服务器端：配置、下发，游戏、渠道参数。
客户端：整合所有参数，执行反编译修改逻辑。

#### 使用说明

### 1.  登录
![输入图片说明](https://foruda.gitee.com/images/1676099731195796974/3119e758_334438.png "1.png")

### 2.  游戏配置
![输入图片说明](https://foruda.gitee.com/images/1676099782154926040/6ccdbdae_334438.png "2.png")

### 3.  选取计费渠道
![输入图片说明](https://foruda.gitee.com/images/1676099824946825067/6ddb9008_334438.png "3.png")

### 4.  执行打包修改
![输入图片说明](https://foruda.gitee.com/images/1676099871501687748/5a2f907b_334438.png "4.png")
![输入图片说明](https://foruda.gitee.com/images/1676099885701170181/eb25b23c_334438.png "5.png")

